import React, { Suspense } from 'react'
import { Canvas} from 'react-three-fiber'
import { Physics } from '@react-three/cannon';
import '../styles/FlappyBirdScene.css'
import PipesManager from './pipes/PipesManager';
import useStore from '../hooks/useStore';
import GameOver from './GameOver';
import FlappyBirdBackground from './planes/FlappyBirdBackground';
import SimpleBird from './birds/SimpleBird';
import Bird from './birds/Bird';
import PlanesManager from './planes/PlanesManager';

const FlappyBirdScene = () => {

    const { gameOver,started } = useStore();

    return (
        <Canvas  shadowMap>
            <ambientLight intensity={0.5} />
            <spotLight position={[-10, 15, 50]} angle={0.6} />
            <Suspense fallback={null}>
                <FlappyBirdBackground />
                </Suspense>
            {gameOver ? <GameOver />: (!started ?
                <SimpleBird /> : 
                <Physics gravity={[0, -25, 0]} >
                    <Suspense fallback={null}>
                        <PipesManager />
                        <Bird/>
                        <PlanesManager />
                    </Suspense>
                </Physics>)}
        </Canvas>
    )
}

export default FlappyBirdScene
