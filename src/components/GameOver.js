import React from 'react';
import { Html } from 'drei';
import useStore from '../hooks/useStore';
import { useThree } from 'react-three-fiber';

const GameOver = () => {

    const { restart, score } = useStore();
    const { viewport } = useThree();
    return (
        <Html position={[-1, 2, 0]} scale={[viewport.width / 2, viewport.height / 2]} >
            <div className='startBox'>
                <span className='startBox__title'>Flappy bird</span>
                <span className='startBox__title'>Score: {score}</span>
                <button className='startBox__startButton'
                    onClick={restart}
                >Start over</button>
            </div>
        </Html>
    )
}

export default GameOver
