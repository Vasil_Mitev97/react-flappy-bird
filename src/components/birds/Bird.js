import { useCylinder,} from '@react-three/cannon';
import React, { useMemo } from 'react'
import { useThree } from 'react-three-fiber'
import * as THREE from 'three'
import bird from '../../580b57fbd9996e24bc43bb6a.png'
import useClick from '../../hooks/useClick';
import useKey from '../../hooks/useKey';
import useStore from '../../hooks/useStore';

const Bird = () => {

    const { setGameOver,stopGame } = useStore();
    const { viewport } = useThree();
    const texture = useMemo(() => new THREE.TextureLoader().load(bird), []);
    const positionX = useMemo(() => -viewport.width / 3, [viewport.width])
    const velocityY = useMemo(() => viewport.height / 1.08, [viewport.height]);

    const [ref, api] = useCylinder(() => ({
        type: 'Dynamic',
        position: [positionX, 0, 0],
        rotation: [Math.PI / 2, Math.PI / 2, 0],
        args: [viewport.height / 16, viewport.height / 16, 0, 40, 20, false, 4],
        mass: 200,
        
        onCollide: () => {
            api.position.set(positionX, 0, 0)
            setGameOver();
            stopGame();
        }
    }))

    const handleClick = (e) => {
        e.stopPropagation();
        api.velocity.set(0, velocityY, 0)
    }
    useKey('Space', handleClick);
    useClick(handleClick);

    return (
        <mesh ref={ref} castShadow rotation={[Math.PI / 2, Math.PI / 2, 0]}>
            <cylinderBufferGeometry attach="geometry"
                args={[viewport.height / 16, viewport.height / 16, 0, 40, 20, false, 4]}
            />
            <meshStandardMaterial attach="material" transparent map={texture} />
        </mesh>
    )
}

export default Bird