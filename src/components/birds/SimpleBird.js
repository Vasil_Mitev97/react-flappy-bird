import React, { useMemo, useRef } from 'react'
import { useFrame, useThree } from 'react-three-fiber';
import * as THREE from 'three'
import bird from '../../580b57fbd9996e24bc43bb6a.png'
import useClick from '../../hooks/useClick';
import useKey from '../../hooks/useKey';
import useStore from '../../hooks/useStore';

const SimpleBird = () => {
    
    const { startGame } = useStore();
    const { viewport } = useThree();
    const ref = useRef();
    const texture = useMemo(() => new THREE.TextureLoader().load(bird), []);
    const positionX = useMemo(() => -viewport.width / 3, [viewport.width])
    const handleClick = (e) => {
        e.stopPropagation();
        startGame();
    }
    useKey('Space', handleClick);
    
    useClick(handleClick);

    useFrame((state) => {
        const time = state.clock.getElapsedTime()
        if (ref.current) {
            
            ref.current.position.y = Math.sin(time * 5) / 10
        }
    })
    return (
        <mesh ref={ref}
            castShadow
            rotation={[Math.PI / 2, Math.PI / 2, 0]}
            position={[positionX, 0, 0]}>
            <cylinderBufferGeometry attach="geometry"
                args={[viewport.height / 16, viewport.height / 16, 0, 40, 20, false, 4]}
            />
            <meshStandardMaterial attach="material" transparent
                map={texture}
            />
        </mesh>
    )
}

export default SimpleBird
