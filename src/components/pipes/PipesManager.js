import React from 'react'
import { useThree } from 'react-three-fiber';
import Pipe from './Pipe'

const PipesManager = () => {
    const { viewport } = useThree();
    
    return (
        <group>
            <Pipe startingPosition={[viewport.width / 2, 2, 0]} />
            <Pipe startingPosition={[viewport.width * 1.1, 3, 0]} />
            <Pipe startingPosition={[viewport.width * 1.7, 3, 0]} />
        </group>
    )
}

export default React.memo(PipesManager)