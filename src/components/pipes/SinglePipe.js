import { useCompoundBody } from '@react-three/cannon';
import React, { useEffect, useRef } from 'react'
import { useFrame, useThree } from 'react-three-fiber';
import useStore from '../../hooks/useStore';

export const SinglePipe = ({ position = [0, 0, 0], rotation = [0, 0, 0], arr = [], distance }) => {
  const { increaseScore } = useStore();
  const { viewport } = useThree();
  const [ref, api] = useCompoundBody(() => ({
    type: 'Static',
    scale: [viewport.width / 14, viewport.height / 5, 1],
    rotation,
    position,
    shapes: [
      {
        type: 'Cylinder',
        position: [0, 0, 0],
        rotation: [0, 0, 0],
        args: [1, 1, 1, 20],
      },
      {
        type: 'Cylinder',
        position: [0, 3, 0],
        rotation: [0, 0, 0],
        args: [0.8, 0.8, 5, 25],
      },
    ]
  }));
  const pos = useRef(position)
  const randomRef = useRef(0);
  
  useEffect(() => api.position.subscribe((v) => (pos.current = v)), [api.position])
  
  useFrame(() => {
    if (pos.current[0] + 1.5 < -viewport.width / 2) {

      if (randomRef.current > arr.length - 1) { 
        randomRef.current = 0;
      }
      if (rotation[2] === 0) {
        api.position.set(viewport.width * 1.1, arr[randomRef.current], pos.current[2]);
        increaseScore();
      }
      else {
        api.position.set(viewport.width * 1.1, arr[randomRef.current] - distance, pos.current[2]);
      }
      randomRef.current++;
    }
    else {
      api.position.set(pos.current[0] - viewport.width / 100, pos.current[1], pos.current[2]);
    }
  })
  return (
    <group ref={ref} position={pos} rotation={rotation} scale={[viewport.width / 14, viewport.height / 5, 1]}>
      <mesh position={[0, 0, 0]} >
        <cylinderBufferGeometry attach='geometry' args={[1, 1, 1, 20]} />
        <meshStandardMaterial attach='material' color='green' />
      </mesh>
      <mesh position={[0, 3, 0]} >
        <cylinderBufferGeometry attach='geometry' args={[0.8, 0.8, 5, 25]} />
        <meshStandardMaterial attach='material' color='green' />
      </mesh>
    </group>
  )
}
