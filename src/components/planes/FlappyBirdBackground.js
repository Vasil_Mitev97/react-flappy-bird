import { Html } from 'drei';
import React, { useMemo } from 'react'
import { useThree } from 'react-three-fiber'
import * as THREE from 'three'
import backgroundImage from '../../background.png';
import useStore from '../../hooks/useStore';

const FlappyBirdBackground = () => {

    const { score } = useStore();
    const { viewport } = useThree();
    const texture = useMemo(() => new THREE.TextureLoader().load(backgroundImage), []);
  
    return (
        <group>
            <mesh position={[0, 0, -2]} receiveShadow>
                <planeBufferGeometry attach='geometry' args={[1.4 * viewport.width, 1.4 * viewport.height]} />
                <meshBasicMaterial attach='material' map={texture} />
            </mesh>
            <Html position={[0, 0, -1]}>
                <span>{score}</span>
            </Html>
        </group>
    )
}

export default FlappyBirdBackground