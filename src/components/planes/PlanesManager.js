import React from 'react'
import { useThree } from 'react-three-fiber';
import Plane from './Plane';

const PlanesManager = () => {
    const { viewport } = useThree();
    return (
        <group>
            <Plane position={[0, viewport.height * 0.7, 0]} />
            <Plane position={[0, -viewport.height * 0.7, 0]} />
        </group>
    )
}
export default PlanesManager