import { useEffect } from "react";
const useClick = ( func = () => { }) => {
  //const [pressed, setPressed] = useState(false);
  
    const onDown = e => {
        // e.stopPropagation();
        func(e)
  };

  useEffect(() => {
    window.addEventListener('mousedown', onDown);
    
    return () => {
      window.removeEventListener('mousedown', onDown);
    };
  });
 // return pressed;
};

export default useClick
