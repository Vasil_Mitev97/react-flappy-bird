import { useEffect, useState } from "react";

const useKey = (key, func = () => { }) => {
  const [pressed, setPressed] = useState(false);
  const match = e => key.toLowerCase() === e.key.toLowerCase();
    const onDown = e => {
    
        if (match(e) || e.code === 'Space') {
       
          setPressed(false);
          func(e);
      }
  };
  const onUp = e => {
      if (match(e)||e.code==='Space') {
          setPressed(false);
         // func();
      }
  };
  useEffect(() => {
    window.addEventListener('keydown', onDown);
    window.addEventListener('keyup', onUp);
    return () => {
      window.removeEventListener('keydown', onDown);
      window.removeEventListener('keyup', onUp);
    };
  });
  return pressed;
};

export default useKey;