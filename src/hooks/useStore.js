import create from "zustand";

const [useStore] = create(set => ({
    score:0,
    increaseScore: () => set(state => ({ score: state.score + 1 })),
    resetScore: () => set({ score: 0 }),
    started: false,
    startGame: () => set({ started: true }),
    stopGame: () => set({ started: false }),
    gameOver: false,
    setGameOver: () => set(state => ({ gameOver: !state.gameOver })),
    restart: () => set(state => ({
        started: false,
        gameOver: false,
        score:0
     }))
}))

export default useStore;